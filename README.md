# recipe-app-api-proxy

nginx proxy app for our recipe app API

## Usage


### Environment Variables

* `Listen_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - port of the app to forward requests to (default: `9000`)